from node:13.8

#CMD apt update && apt upgrade -y && apt install build-essential git -y
#mío
RUN apt update && apt install gettext-base -y
RUN envsubst -V

RUN useradd -ms /bin/bash syncuser

USER syncuser
WORKDIR /home/syncuser

RUN git clone -b 3.0 https://github.com/calzoneman/sync

WORKDIR /home/syncuser/sync

RUN npm install

#mío
#reemplazar variables de entorno (en build! no en ejecución!)
ARG MYSQL_PASSWORD
ARG COOKIE_SECRET
ARG YOUTUBE_V3_KEY
COPY ./config.tmpl.yaml ./config.tmpl.yaml
RUN envsubst < ./config.tmpl.yaml > ./config.yaml
#para usar config sin reemplazo de variables hacer directamente:
#COPY ./config.yaml ./config.yaml

CMD [ "node", "index.js" ]
