// Generador de hash de contraseña compatible con base de datos CyTube
// node ./test-scripts/hash-password.js <contraseña> [<usuario>]

var bcrypt = require("bcrypt");
//console.log(process.argv)
if (process.argv.length < 3) throw new Error('Falta argumento')
const plainPass = process.argv[2]
const userName = process.argv.length < 4 ? 'XXXX' : process.argv[3]
// esta es la forma oficial como se ve en el fuente de cytube en src/database/accounts.js
bcrypt.hash(plainPass, 10, function (err, hash) {
    if (err) {
        throw err
    }
    console.log(hash)
    let sql = `update users set password="${hash}" where name="${userName}";`
    console.log(sql)
    console.log(`docker exec -it cytube-docker_mysql_1 mysql -ucytube3 -psuper_secure_password cytube3 -e '${sql}'`)
    console.log(`docker exec -it cytube-docker_mysql_1 mysql -ucytube3 -psuper_secure_password cytube3 -e '${sql}select ROW_COUNT() as updated'`)
})
